# The CSS framework with swagger

[Changelog](CHANGELOG.md)

## Use it in your project

```
npm install git+https://brombrommert@bitbucket.org/thinkbright/tb-swagger.git#0.2.3
```

Include it in your SASS poject like:

```
@import './node_modules/tb-swagger/main';
```

Or with webpack:

```
import 'tb-swagger';
```

## Development

```
git clone git@bitbucket.org:thinkbright/tb-swagger.git
```

Start browser with test suite using shipmate and docker:

```
shipmate deploy development
shipmate attach node
npm install
npm test:build
npm start
```

Or, start browser with test suite on your local machine:

```
npm install
npm test:build
npm start
```