/*
    Grid System

    The grid system uses mixins to perform grid magic.
    You might want to override default grid-related variables; defaults are:
      $gutter-small:  1rem;
      $gutter-medium: 2rem;
      $gutter-large:  3rem;

      $grid-columns:  12;

    Use it with easy-to-reason-about queries, like so:

    // create column in grid system with ~50% width
    @include grid-column(6)
    // or
    @include grid-column(1 of 2)

    // Sometimes you want some controls over gutters
    @include grid-column(1 of 2 container)          // include gutter as
    @include grid-column(1 of 2 gutter)             // include gutter
    @include grid-column(1 of 2 no-container)       // include gutter
    @include grid-column(1 of 2 parent-container)   // include gutter
    @include grid-column(1 of 2 seamless)           // omit gutters in width

    // add horizontal padding to a column (or other element)
    @include grid-pad(horizontal)
    @include grid-pad(horizontal 2)         // 2 columns of $grid-columns
    @include grid-pad(horizontal 1 of 4)    // 2 columns of 4 $grid-columns

    $variables
      $query                  - columns | columns of total | direction | gutterOption
      $queryMap               - SASS map with:
                                  columns: number
                                  direction: top | right | bottom | left | horizontal | vertical
                                  grid-columns: number
                                  gutter: container | gutter | no-container | parent-container | seamless

    @fuctions
      grid-width              - ($query) => width
      _grid-parse-query       - ($query) => $queryMap
      _grid-width             - ($columns, $grid-columns, $gutterOption) => width

    @mixins
      grid-column             - ($query) => create grid column
      grid-container          - ($query) => create grid container
      grid-pad                - ($query) => add padding to element
      grid-row                - ($query) => create grid row
      grid-space              - ($query) => add margin to element
      _grid-directional-prop  - ($cssProperty, $queryMap) => create css props with directions
*/

$grid-gutter: map-get($grid-gutters, small);

@function _grid-parse-query($query, $defaults: ()) {
  $_allowed_directions: top right left bottom horizontal vertical;
  $_allowed_guttersOptions: seamless;

  $_queryMap: map-merge((
    columns: 1,
    direction: null,
    grid-columns: $grid-columns,
    gutters: 0,
    guttersOption: null
  ), $defaults);

  $_matched_columns: false;
  @each $arg in $query {
    $_index: index($query, $arg);

    @if type-of($arg) == number
    and not $_matched_columns
    and nth($query, if($_index > 1, $_index - 1, $_index)) != gutter {
      $_queryMap: map-merge($_queryMap, (columns: $arg));
      $_matched_columns: true;
    }
  }

    @if _grid-query-has($query, of) {
      $_columns: nth($query, index($query, of) - 1);
      $_grid-columns: nth($query, index($query, of) + 1);

      $_queryMap: map-merge($_queryMap, (
        columns: $_columns,
        grid-columns: $_grid-columns
      ));
    }

  @if _grid-query-has($query, $_allowed_directions) {
    $_direction: _grid-query-get($query, $_allowed_directions);
    $_queryMap: map-merge($_queryMap, (direction: $_direction));
  }

  @if _grid-query-has($query, gutter) {
    $_gutters: 1;

    @if length($query) > index($query, gutter)
        and type-of(nth($query, index($query, gutter) + 1)) == number {
      $_gutters: nth($query, index($query, gutter) + 1);
    }

    $_queryMap: map-merge($_queryMap, (gutters: $_gutters));
  }

  @if _grid-query-has($query, $_allowed_guttersOptions) {
    $_columns: map-get($_queryMap, columns);
    $_grid-columns: map-get($_queryMap, grid-columns);
    $_gutters: map-get($_queryMap, gutters);

    $_queryMap: map-merge($_queryMap, (
      guttersOption: _grid-query-get($query, $_allowed_guttersOptions)
    ));

    @if map-get($_queryMap, guttersOption) == seamless
    and not _grid-query-has($query, gutter -gutter)
    and $_gutters == 0 {
      $_queryMap: map-merge($_queryMap, (
        gutters: 1 + ($_columns / $_grid-columns)
      ));
    }
  }

  @return $_queryMap;
}

@function _grid-query-get($query, $thing) {
  $_match: null;

  @if (type-of($thing) == string or type-of($thing) == number)
  and index($query, $thing) != null {
    $_match: nth($query, index($query, $thing));
  } @else if type-of($thing) == list {
    @each $item in $thing {
      @if index($query, $item) != null {
        $_match: nth($query, index($query, $item));
      }
    }
  }

  @return $_match;
}

@function _grid-query-has($query, $thing) {
  @return _grid-query-get($query, $thing) != null;
}

@function _grid-width($columns: 1, $grid-columns: $grid-columns, $gutters: 0) {
  @if $columns == null or $grid-columns == null or $gutters == null {
    @error 'Parameter cannot be null:'
      columns $columns
      grid-columns $grid-columns
      gutters $gutters;
  }

  // HACK fixes weird rounding errors in IE 11
  $gutters: $gutters - 0.00001;

  @return calc(
      #{percentage($columns / $grid-columns)}
    + #{($grid-gutter + ($grid-gutter * $columns / $grid-columns)) * -1}
    + #{$grid-gutter * $gutters}
  );
}

// Grid width calculates the width of an element, dependent on the amount of
// columns passed to the function.
@function grid-width($query: null) {
  $_queryMap: _grid-parse-query($query);

  @return _grid-width(
    map-get($_queryMap, columns),
    map-get($_queryMap, grid-columns),
    map-get($_queryMap, gutters)
  );
}


@mixin _grid-directional-prop($property, $queryMap) {
  $_direction: map-get($queryMap, direction);
  $_width: _grid-width(
    map-get($queryMap, columns),
    map-get($queryMap, grid-columns),
    map-get($queryMap, gutters)
  );

  @if $_direction == null {
    #{$property}: $_width;
  } @else if $_direction == horizontal {
    #{$property}-left: $_width;
    #{$property}-right: $_width;
  } @else if $_direction == vertical {
    #{$property}-bottom: $_width;
    #{$property}-left: $_width;
  } @else {
    #{$property}-#{map-get($queryMap, direction)}: $_width;
  }
}

@mixin grid-column($query: null) {
  float: left;

  @if not _grid-query-has($query, seamless) {
    margin-left: map-get($grid-gutters, small);
  }

  width: grid-width($query);
}

@mixin grid-container() {
  margin-left: auto;
  margin-right: auto;
  max-width: calc(#{$grid-max-width} - #{$grid-gutter});
}

@mixin grid-push($query: null) {
  $_defaults: (gutters: 2);
  $_queryMap: _grid-parse-query($query, $_defaults);

  @include _grid-directional-prop(
    margin,
    $_queryMap
  );
}

@mixin grid-pad($query: null) {
  $_defaults: (gutters: 1);
  $_queryMap: _grid-parse-query($query, $_defaults);

  @include _grid-directional-prop(
    padding,
    $_queryMap
  );
}

@mixin grid-row($query: null) {
  @include clearfix;

  display: flex;
  flex-wrap: wrap;

  @if _grid-query-has($query, gutter) {
    padding-left: $grid-gutter;
    padding-right: $grid-gutter;
  }
}

