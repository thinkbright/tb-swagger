# Changelog

## v0.2.3

- Removed `max-width` declaration in favor of just `width`. This fixes an issue with iOS9 Safari where grid columns did get correct widths, but were never floated next to each other.